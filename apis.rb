require_relative 'get_data.rb'

class Apis
  attr_accessor :response, :hash_temp, :final_array

  def initialize
    @hash_temp = {}
    @final_array = []
  end

  def start
    connect
    make_hash
  end

  def connect
    @response = GetData.start @url
  end



end


