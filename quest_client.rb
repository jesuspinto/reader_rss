require_relative 'browser.rb'

  class QuestClient<Browser
  attr_accessor :parameters, :answer1, :answer2, :api_names, :list_class

  def initialize
    @list_class = []
    api_list
  end

  def start
    b = Browser.new
    b.dynamic_class_loader @parameters
    b.show
  end

  def api_list
    @api_names = Dir.glob('apis/*').each {|archivo|
      p archivo.sub!("apis/","")
    }
  end


  def ask_client
    #p 123123
    ask1
    ask2
    @parameters
    start
  end

  def ask1
    system("clear")
    puts '¿Como desea ver sus noticias?'
    puts '1 => Ordenadas por fecha'
    puts '2 => Ordenadas por pagina web'
    puts 'Escriba un numero y presione enter: '
    @answer1 = gets.chomp.to_i
  end

  def ask2
    system("clear")
    if @answer1==1
      @parameters = [true, nil]
    else
      puts 'Seleccione el diario de su preferencia'
      @api_names.size.times {|i| puts "Presione #{i+1} => para ver las noticias de #{@api_names[i]}"}
      j = gets.chomp.to_i-1
      @parameters = [false, "apis/#{@api_names[j]}"]
      #p @api_names[j]

    end
  end

end


