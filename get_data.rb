require 'rest-client'
require 'json'

class GetData
  attr_accessor :response, :url

  def self.start url
    @url = url
    get_response
    parse_response
  end

  def self.get_response
    @response = RestClient.get(@url)
  end

  def self.parse_response
    @response =  JSON.parse @response, {:symbolize_names => true}
  end
end


