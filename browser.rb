require_relative 'apis.rb'
require_relative "news_paper.rb"
require 'colorize'
require 'launchy'

class Browser
  attr_accessor :actual_page, :news_paper, :action, :ready


  def initialize
    @ready = true
    @actual_page = 0
    @news_paper = NewsPaper.new
  end

  def dynamic_class_loader parameters
      @parameters = parameters
      @list_class = []
      if @parameters == [true, nil]

        Dir.glob("apis/*.rb") do |file_name|
          puts "Procesando \"#{file_name}\""

          class_name = file_name.sub!("apis/","").chomp!(".rb").split("_").inject("") {|cn, ss| cn += ss.capitalize}
          #p class_name
          require_relative "apis/#{file_name}"
          found_class = Object.const_get("#{class_name}")

          @news_paper.daily_news= @news_paper.daily_news + found_class.new.start
          @list_class.push(class_name)
          #p @news_paper.daily_news.to_s
        end
      else
        class_name = @parameters[1].sub!("apis/","").chomp!(".rb").split("_").inject("") {|cn, ss| cn += ss.capitalize}
        puts "Procesando \"#{@parameters[1]}\""
        require_relative "apis/#{@parameters[1]}.rb"
        found_class = Object.const_get("#{class_name}")
        @news_paper.daily_news = found_class.new.start
        @list_class.push(class_name)
        p @news_paper.daily_news.to_s
      end
  end

  def questions
    puts 'Escriba una de las siguentes opciones: '
    puts "#{@actual_page!=0 ? "a => pagina anterior, ": nil} #{@actual_page < @news_paper.daily_news.size - @news_paper.daily_news.size%5 ? "s => siguiente, ": nil} q => salir y luego presione Enter"
    puts "Opcion: #{@action = gets.chomp}"

    @action == 'a' && @actual_page > 0 ? @actual_page -= 5 : nil
    @action == 's' && @actual_page < @news_paper.daily_news.size - @news_paper.daily_news.size%5? @actual_page += 5 : nil
    @action == 'q' ? @ready = false: nil
    if @action.to_i - 1 >= 0 && @action.to_i - 1 <= 4
      Launchy.open(@news_paper.daily_news[@actual_page+@action.to_i-1][:link])
      system("clear")
      p "Espere mientras se abre el navegador"
      sleep(8)
    end
  end

  def show_news
    system ('clear')
    p '##############################################################'
    p '##############################################################'
    #p @news_paper.daily_news.size%5
    if @actual_page < @news_paper.daily_news.size - @news_paper.daily_news.size%5
      count_times = 5
    else
      count_times = @news_paper.daily_news.size%5
    end
      count_times.times do |j|
      4.times do |i|
        title = [:Titulo, :Autor, :Fecha, :Enlace]
        key=[:title, :author, :date, :link]
      p "#{title[i]} = #{@news_paper.daily_news[@actual_page+j][key[i]]}"
      end
      p "Para visitar este enlace escriba #{j + 1} y luego presione Enter"
    p '##############################################################'

    end
    p '##############################################################'
    p "                          -#{(5+@actual_page)/5}-                                 "
    puts
  end

  def show
    @news_paper.daily_news.sort_by!{|obj| obj[:date]  }
    while @ready == true

      show_news

      questions

    end
  end
end
