require 'date'
require_relative '../apis.rb'


class Digg < Apis

  attr_accessor :name, :url, :layers, :bodies, :keys_to_dig, :keys

  def initialize
    super
    @name = 'digg'
    @url = 'http://digg.com/api/news/popular.json'
    @layers = [:data]
    @bodies = [:feed]
    @keys_to_dig = [:content]
    @keys = [:title,  :author, :date_published, :original_url]
  end


  def make_hash
    if @bodies != nil
      #p bodies.to_s
      @bodies.size.times do |i|
        @response[:data][@bodies[i]].size.times do |j|
          #p @response[@bodies[i]]
          @hash_temp = {title:@response[:data][@bodies[i]][j][:content][@keys[0]]}
          @hash_temp = @hash_temp.merge({author:@response[:data][@bodies[i]][j][:content][@keys[1]]})
          date1 = Time.at(@response[:data][@bodies[i]][j][@keys[2]].to_i).strftime("%d/%m/%Y %k:%M")
          @hash_temp = @hash_temp.merge({date:date1})
          #"Fecha #{fecha.mday}/#{fecha.mon}/#{fecha.year}"
          @hash_temp = @hash_temp.merge({link:@response[:data][@bodies[i]][j][:content][@keys[3]]})
          @final_array<<@hash_temp
        end
      end
    end
     @final_array
  end


end

