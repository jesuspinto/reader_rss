require 'date'
require_relative '../apis.rb'

class Mashables < Apis

  attr_accessor :name, :url, :bodies, :keys

  def initialize
    super
    @name = 'mashables'
    @url = 'https://mashable.com/stories.json'
    @bodies = [:new, :rising, :hot]
    @keys = [:title, :author, :post_date, :link]
  end

  def make_hash
    if @bodies != nil
      #p bodies.to_s
      @bodies.size.times do |i|
        @response[@bodies[i]].size.times do |j|
          #p @response[@bodies[i]]
          @hash_temp = {title:@response[@bodies[i]][j][@keys[0]]}
          @hash_temp = @hash_temp.merge({author:@response[@bodies[i]][j][@keys[1]]})
          #0--3 56 89 11..15
          #2018-08-13T16:09:19+00:00
          date1 = @response[@bodies[i]][j][@keys[2]].to_s
          date1 = date1[8..9] + '/' + date1[5..6] + '/' + date1[0..3] + '  ' + date1[11..15]
          #p date1
          #date1 = Date.parse(date1)
          @hash_temp = @hash_temp.merge({date:date1})
          @hash_temp = @hash_temp.merge({link:@response[@bodies[i]][j][@keys[3]]})
          @final_array<<@hash_temp
        end
      end
    end
     @final_array
  end

end

