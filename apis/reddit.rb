require 'date'
require_relative '../apis.rb'

class Reddit < Apis

  attr_accessor :name, :url, :bodies, :keys

  def initialize
    super
    @name = 'reddit'
    @url = 'https://www.reddit.com/.json'
    @bodies = [:children]
    @keys = [:title, :author, :created_utc, :url]
  end

  def make_hash
    if @bodies != nil
      #p bodies.to_s
      @bodies.size.times do |i|
        @response[:data][@bodies[i]].size.times do |j|
          #p @response[@bodies[i]]
          @hash_temp = {title:@response[:data][@bodies[i]][j][:data][@keys[0]]}
          @hash_temp = @hash_temp.merge({author:@response[:data][@bodies[i]][j][:data][@keys[1]]})


          date1 = Time.at(@response[:data][@bodies[i]][j][:data][@keys[2]].to_i).strftime("%d/%m/%Y %k:%M")




          @hash_temp = @hash_temp.merge({date:date1})
          @hash_temp = @hash_temp.merge({link:@response[:data][@bodies[i]][j][:data][@keys[3]]})
          @final_array<<@hash_temp
        end
      end
    end
     @final_array
  end

end

